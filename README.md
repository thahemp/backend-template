Empty backend template

This provides a docker environment with containers for MongoDB, MongoDB Express (database web viewer), and golang backend API server.

1) From the project root run 'docker-compose up -d'
2) Enter the backend/src directory and run './run.sh'
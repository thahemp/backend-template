package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	// "time"
)

///
//	MongoDB access
type handlerParams struct {
	db *testDB
}


type TestMessage struct {
	Type string `json:"type"`
	Token string `json:"token"`
	User string `json:"user"`
}

///
//	Handler function for client requests. TestMessage JSON data is provided by a client. After
//	database operations are performed, a TestMessage response is generated and sent to the client.
func (params *handlerParams) authRequestHandler(w http.ResponseWriter, r *http.Request) {
	
	setupResponse(&w, r)
	if(*r).Method == "OPTIONS" {
		return
	}

	if r.Body == nil {
		http.Error(w, "No request body for POST", http.StatusBadRequest)
		return
	}

	// Unmarshal request body into our AuthMessage type
	var request TestMessage
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var response TestMessage

	switch messageType := request.Type; messageType {
	case "handshake":
		user := User{Token: request.Token}
		result, err := params.db.userHandshake(&user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusForbidden)
			return
		}

		response = TestMessage{
			Type: "handshake", 
			Token: result.Token,
			User: request.User,
		}
	case "login":
	case "logout":
	}

	// Build the response to the client
	prettyJSON, err := json.MarshalIndent(response, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// fmt.Println(string(prettyJSON))

	// Write the response to the client
	fmt.Fprintf(w, "%s\n", string(prettyJSON))

} //authRequestHandler


func httpListenBlocking(db *testDB) {
	params := &handlerParams{db: db}

	mux := http.NewServeMux()

	// example route
	mux.HandleFunc("/signin", params.authRequestHandler)

	log.Println("Starting HTTP auth server on port 7343")

	err := http.ListenAndServe(":7343", mux)
	if err != nil {
		log.Fatalf("Failed to start http server on port 7343: %s", err.Error())
	}
} //httpListenBlocking

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

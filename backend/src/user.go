package main

import (
	"time"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {

	ID							primitive.ObjectID	`bson:"_id,omitempty"`

	Token						string				`json:"token" bson:"token"`
	User						string				`json:"user" bson:"user"`
	Password					string				`json:"password" bson:"password"`
	TokenCreateTime				*time.Time			`json:"tokenCreateTime" bson:"tokenCreateTime"`
	TokenExpireTime				*time.Time			`json:"tokenExpireTime" bson:"tokenExpireTime"`

}
package main

import (
	"context"
	"log"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// This struct functions as a cache for our various mongodb handles
type testDB struct {
	handle *mongo.Database
	client *mongo.Client

	testCollection  *mongo.Collection
}

func (db *testDB) open() {
	mongoURI := "mongodb://user:password@localhost:27017"
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatalf("failed to create client: %s\n", err.Error())
	}

	if err := client.Connect(context.TODO()); err != nil {
		log.Fatalf("failed to connect: %s\n", err.Error())
	}

	handle := client.Database("test-database")
	if err := client.Ping(context.TODO(), nil); err != nil {
		log.Fatalf("failed to ping database: %s\n", err.Error())
	}

	log.Printf("successfully connected to database\n")

	db.client = client
	db.handle = handle

	db.testCollection = handle.Collection("test-collection")

} //open

// Close the connection to mongodb
func (db *testDB) close() {
	if err := db.client.Disconnect(context.TODO()); err != nil {
		log.Fatalf("failed to close database: %v", err)
	}

} //close

// Add database query functionality here: find/update documents etc
func (db *testDB) userHandshake(u *User) (*User, error) {
	var result *User

	// Create a mongodb filter with the User info provided
	filter := bson.M{"token": u.Token}

	// Search the AuthDB for the User.token provided
	mgoSingleRes := db.testCollection.FindOne(context.TODO(), filter)
	if mgoSingleRes.Err() != nil {
		return nil, fmt.Errorf("token invalid: %s", u.Token)
	}

	// Decode the bson from testDB into our User object type
	err := mgoSingleRes.Decode(&result)
	if err != nil {
		return nil, fmt.Errorf("failed to decode token match result from testDB: %v", err)
	}

	// Check token expiration
	if result.TokenExpireTime.Before(time.Now()) {
		return nil, fmt.Errorf("token is expired: %s", result.Token)
	}

	// From here on we have a valid user that needs a refreshed token.

	// Supplied token was valid. Generate a fresh one for the user.
	result, err = db.createNewUserToken(filter)
	if err != nil {
		return nil, fmt.Errorf("failed to create new token for user: %v", u)
	}

	return result, nil
}
///
//	New user token creation. Feed the bson filter required to locate the appropriate user. A new
//	token is generated and the testDB is updated with the token, create, and expire times.
func (db *testDB) createNewUserToken(filter interface{}) (*User, error) {

	// Set the User token expire time here
	expireTimeInMinutes := 60

	// Get necessary time values for creating a new token
	createTime := time.Now()
	expireTime := createTime.Add(time.Minute * time.Duration(expireTimeInMinutes))

	// Using the creation time as the string used to generate the token for uniqueness
	token, err := HashPassword(createTime.String())
	if err != nil {
		return nil, err
	}

	// Create an update filter to update token-related fields for the User in the testDB
	update := bson.M{"$set": bson.M{"token": token, "tokenCreateTime": createTime, "tokenExpireTime": expireTime}}

	// Update AuthDB with the new token data
	updateResult, err := db.testCollection.UpdateOne(context.TODO(), filter, update, options.Update().SetUpsert(false))
	if err != nil || updateResult.ModifiedCount == 0 {
		return nil, err
	}

	// Find the updated User document using the newly generated token
	var result *User

	filter = bson.M{"token": token}

	mgoSingleRes := db.testCollection.FindOne(context.TODO(), filter)
	if mgoSingleRes.Err() != nil {
		return nil, fmt.Errorf("failed to find user using new token: %s", token)
	}

	err = mgoSingleRes.Decode(&result)
	if err != nil {
		return nil, fmt.Errorf("failed to decode new token match: %v", err)
	}

	return result, nil
}

// // Get number of records in database
// func (db *testDB) size() (int64, error) {
// 	return db.testCollection.CountDocuments(context.TODO(), nil, nil)
// }
